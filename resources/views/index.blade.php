<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Inicio</title>
</head>
<body>
<div id="app">
    <login-component></login-component>
</div>

<!-- Incluye el script de Vue compilado por Laravel Mix -->
@vite(['resources/js/app.js'])
</body>
</html>
