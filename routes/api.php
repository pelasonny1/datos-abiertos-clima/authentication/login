<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Route;

Route::get('/clima', function (Request $request) {
    $response = Http::get("http://localhost:8082/temperatura");

    return $response->json();
});
