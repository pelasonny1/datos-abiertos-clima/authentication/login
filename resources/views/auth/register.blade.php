@extends('auth.layouts.app')

@section('content')
    <div id="app">
        <register-component></register-component>
    </div>

    <script src="{{ mix('js/app.js') }}"></script>
@endsection
