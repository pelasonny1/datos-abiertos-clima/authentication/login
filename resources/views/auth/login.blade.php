@extends('auth.layouts.app')

@section('content')
    <div id="app">
        <login-component></login-component>
    </div>
@endsection

@vite(['resources/js/app.js'])
